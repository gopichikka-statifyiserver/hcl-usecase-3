package com.gpay.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ctc.wstx.shaded.msv_core.reader.xmlschema.AnyState;
import com.gpay.dto.CustomerCreatedResponseDto;
import com.gpay.dto.CustomerRegistrationDto;
import com.gpay.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.gpay.feignclient.UskBankClient;
import com.gpay.model.Customer;
import com.gpay.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private UskBankClient client;
	
	
	@Test
	void registerCustomer_HappyPath() {
		when(client.customerValidationWithPhoneNumber(anyString())).thenReturn(buildCustomerValidationSuccessResponse());
		when(customerRepository.save(any())).thenReturn(new Customer(1L, "Arya", "Stark", 19, "9874561235", "arya.stark@gmail.com",
			"arya@123"));
		CustomerRegistrationDto customerRegistrationRequest = buildCustomerRegistrationRequest();
		 CustomerCreatedResponseDto customerCreatedResponse = customerServiceImpl.registerCustomer(customerRegistrationRequest);
		 assertEquals(customerRegistrationRequest.getPhoneNumber(), customerCreatedResponse.getPhoneNumber());
	}


	private CustomerRegistrationDto buildCustomerRegistrationRequest() {
		
		CustomerRegistrationDto customerRegistrationDto = new CustomerRegistrationDto("Arya", "Stark", 19, "9874561235", "arya.stark@gmail.com", "arya@123");		
		return customerRegistrationDto;
	}


	private ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> buildCustomerValidationSuccessResponse() {
		CustomerValidationWithPhoneNumberResponseDto response = new CustomerValidationWithPhoneNumberResponseDto();
		response.setDoesCustomerExists(true);
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}




}
