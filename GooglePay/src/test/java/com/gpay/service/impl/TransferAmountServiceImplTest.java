package com.gpay.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.gpay.dto.TransactionSuccessResponseDto;
import com.gpay.dto.TransferAmountWithPhoneNumberRequestDto;
import com.gpay.feignclient.UskBankClient;
import com.gpay.model.Customer;
import com.gpay.model.TransactionHistory;
import com.gpay.repository.CustomerRepository;
import com.gpay.repository.TransactionHistoryRepository;

@ExtendWith(MockitoExtension.class)
class TransferAmountServiceImplTest {

	@InjectMocks
	private TransferAmountServiceImpl transferAmountServiceImpl;
	
	@Mock
	private TransactionHistoryRepository transactionHistoryRepository;
	
	@Mock
	private UskBankClient client;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Test
	void getLatestTransactions_Happy_Path() {
		
		List<TransactionHistory> transactionHistoryList = buildTransactionHistoryList();
		when(transactionHistoryRepository.findByCustomerPhoneNumberOrderByTransactionDateDesc(
				anyString(), any())).thenReturn(transactionHistoryList);
		List<TransactionHistory> latestTransactions = transferAmountServiceImpl.getLatestTransactions("9087654321");
		assertEquals(2, latestTransactions.size());
	}
	
	@Test
	void transferAmountThroughPhoneNumbers_Happy_Path() {
		
		List<Customer> customerList = buildCustomerList();
		when(customerRepository.findByPhoneNumberIn(any())).thenReturn(customerList);
		
		
		ResponseEntity<TransactionSuccessResponseDto> transferMoneySuccessResponse = buildTransferMoneySuccessResponse();
		when(client.transferMoneyThroughPhoneNumber(any())).thenReturn(transferMoneySuccessResponse);
		
		TransactionHistory debitTransaction = buildTransactionHistory("9087654321", "9187654321", "DEBIT", 1L);
		when(transactionHistoryRepository.save(any())).thenReturn(debitTransaction);
		
		TransferAmountWithPhoneNumberRequestDto requestdto = new TransferAmountWithPhoneNumberRequestDto("9087654321", "9187654321", BigDecimal.valueOf(1000), new Date(), "random comment");
		TransactionSuccessResponseDto transferAmountSuccessResponse = transferAmountServiceImpl.transferAmountThroughPhoneNumber(requestdto );
		assertEquals(1L, transferAmountSuccessResponse.getDebitTransactionID());
	}

	private TransactionHistory buildTransactionHistory(String fromPhoneNumber, String toPhoneNumber, String transactionType, Long transactionId) {
		TransactionHistory transactionHistory = new TransactionHistory(fromPhoneNumber, toPhoneNumber, BigDecimal.valueOf(1000), transactionType, "random comment");
		transactionHistory.setTransactionID(transactionId);
		return transactionHistory;
	}
	
	private ResponseEntity<TransactionSuccessResponseDto> buildTransferMoneySuccessResponse() {
		TransactionSuccessResponseDto successResponse = new TransactionSuccessResponseDto(1L, 2L);
		return ResponseEntity.status(HttpStatus.OK).body(successResponse);
	}

	private List<Customer> buildCustomerList() {
		List<Customer> customerList = new ArrayList<>();
		Customer customer1 = new Customer(1L, "Ram", "Kumar", 25, "9087654321", "ram.kumar@gmail.com", "ram@123");
		Customer customer2 = new Customer(2L, "Shyam", "Kumar", 26, "9187654321", "shyam.kumar@gmail.com", "shyam@123");
		
		customerList.add(customer1);
		customerList.add(customer2);
		
		return customerList;
	}

	private List<TransactionHistory> buildTransactionHistoryList() {
		List<TransactionHistory> transactionHistoryList = new ArrayList<>();
		TransactionHistory transaction1 = new TransactionHistory(1L,"9087654321", "9187654321", BigDecimal.valueOf(1000), "CREDIT", new Date(), "First Transaction");
		TransactionHistory transaction2 = new TransactionHistory(2L,"9087654321", "9187654321", BigDecimal.valueOf(2000), "DEBIT", new Date(),"Second Transaction");
		transactionHistoryList.add(transaction1);
		transactionHistoryList.add(transaction2);
		return transactionHistoryList;
	}
	

}
