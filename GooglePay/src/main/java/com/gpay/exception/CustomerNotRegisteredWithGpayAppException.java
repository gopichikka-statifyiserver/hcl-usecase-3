package com.gpay.exception;

public class CustomerNotRegisteredWithGpayAppException extends RuntimeException {

	public CustomerNotRegisteredWithGpayAppException(String errorMsg) {
		super(errorMsg);
	}

}
