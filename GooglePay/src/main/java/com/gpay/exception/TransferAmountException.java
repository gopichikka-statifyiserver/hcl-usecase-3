package com.gpay.exception;

import org.springframework.http.ResponseEntity;

import com.gpay.dto.TransactionSuccessResponseDto;

public class TransferAmountException extends RuntimeException {

	public TransferAmountException(ResponseEntity<TransactionSuccessResponseDto> transactionSuccess) {
		
		super(transactionSuccess.getBody().toString());
	}

}
