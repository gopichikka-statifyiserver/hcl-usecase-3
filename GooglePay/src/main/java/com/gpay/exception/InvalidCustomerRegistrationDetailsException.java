package com.gpay.exception;

import org.springframework.http.ResponseEntity;

import com.gpay.dto.CustomerValidationWithPhoneNumberResponseDto;

public class InvalidCustomerRegistrationDetailsException extends RuntimeException {

	public InvalidCustomerRegistrationDetailsException(String errorMsg,
			ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidation) {		
		super(errorMsg);		
	}

	
}
