package com.gpay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;

import com.gpay.model.TransactionHistory;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Long> {	
		public List<TransactionHistory> findFirst10ByCustomerPhoneNumberOrderByTransactionDateDesc(String customerPhoneNumber);
		
		public List<TransactionHistory> findByCustomerPhoneNumberOrderByTransactionDateDesc(String customerPhoneNumber, Pageable pageable);

}
