package com.gpay.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.gpay.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.gpay.dto.TransactionSuccessResponseDto;
import com.gpay.dto.TransferAmountWithPhoneNumberRequestDto;



//@FeignClient(value = "order-service", url = "http://localhost:8001/fundtransferapplication")
@FeignClient(name = "http://BANKING-SERVICE/fundtransferapplication")
public interface UskBankClient {
	@GetMapping("/customers/phonenumber/{phoneNumber}/validate")
	public ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidationWithPhoneNumber(@PathVariable("phoneNumber") String phoneNumber);
	@PutMapping("/transfer-amount-by-phone-number")
	public ResponseEntity<TransactionSuccessResponseDto> transferMoneyThroughPhoneNumber(@RequestBody TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest);
}
