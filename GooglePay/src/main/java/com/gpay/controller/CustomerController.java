package com.gpay.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gpay.dto.CustomerCreatedResponseDto;
import com.gpay.dto.CustomerRegistrationDto;
import com.gpay.service.CustomerService;

@RestController
public class CustomerController {
	Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	CustomerService customerService;

	@PostMapping("/registercustomer")
	public ResponseEntity<CustomerCreatedResponseDto> registerCustomer(@Valid @RequestBody CustomerRegistrationDto customerRegistrationDto) {
		logger.info("CustomerController.registerCustomer :: Entered the method successfully");
		CustomerCreatedResponseDto customerCreatedResponse = customerService.registerCustomer(customerRegistrationDto);
		logger.info("CustomerController.registerCustomer :: Exiting the method with success response");
		return ResponseEntity.status(HttpStatus.OK).body(customerCreatedResponse);
	}

}
