package com.gpay.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.gpay.dto.CustomerCreatedResponseDto;
import com.gpay.dto.CustomerRegistrationDto;
import com.gpay.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.gpay.exception.InvalidCustomerRegistrationDetailsException;
import com.gpay.feignclient.UskBankClient;
import com.gpay.model.Customer;
import com.gpay.repository.CustomerRepository;
import com.gpay.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	UskBankClient client;

	@Override
	public CustomerCreatedResponseDto registerCustomer(CustomerRegistrationDto customerRegistrationDto) {
		logger.info("CustomerServiceImpl.registerCustomer :: Entered the method successfully.");
		
		ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidation = client.customerValidationWithPhoneNumber(customerRegistrationDto.getPhoneNumber());
		if(!customerValidation.getStatusCode().is2xxSuccessful())
			throw new InvalidCustomerRegistrationDetailsException("No phone number found with the given phone number: ", customerValidation);
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRegistrationDto, customer);
		customer = customerRepository.save(customer);
		
		logger.info("CustomerServiceImpl.registerCustomer :: Exiting the method successfully.");
		
		return new CustomerCreatedResponseDto(customer.getCustomerID(), customer.getFirstName(),
												customer.getLastName(), customer.getPhoneNumber());
	}

}
