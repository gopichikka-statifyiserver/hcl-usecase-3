package com.gpay.service;

import javax.validation.Valid;

import com.gpay.dto.CustomerCreatedResponseDto;
import com.gpay.dto.CustomerRegistrationDto;

public interface CustomerService {

	CustomerCreatedResponseDto registerCustomer(CustomerRegistrationDto customerRegistrationDto);

}
