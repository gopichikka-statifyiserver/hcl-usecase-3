package com.gpay.service;

import java.util.List;

import com.gpay.dto.TransactionSuccessResponseDto;
import com.gpay.dto.TransferAmountWithPhoneNumberRequestDto;
import com.gpay.model.TransactionHistory;

public interface TransferAmountService {
	public TransactionSuccessResponseDto transferAmountThroughPhoneNumber(
								TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest);
	
	public List<TransactionHistory> getLatest10Transactions(String customerPhoneNumber);
	public List<TransactionHistory> getLatestTransactions(String customerPhoneNumber);
}
