package com.banking.fundtransfer.service;


import java.util.List;

import com.banking.fundtransfer.dto.TransactionHistoryResponseDto;
import com.banking.fundtransfer.dto.TransactionSuccessResponse;
import com.banking.fundtransfer.dto.TransferAmountWithPhoneNumberRequestDto;
import com.banking.fundtransfer.dto.TransferBalanceRequestDto;
import com.banking.fundtransfer.exception.AccountException;
import com.banking.fundtransfer.exception.AccountNotFoundException;

public interface TransactionService {
	
	public TransactionSuccessResponse transferAmount(TransferBalanceRequestDto transferBalanceRequest) throws AccountException, AccountNotFoundException;
	public List<TransactionHistoryResponseDto> getStatementOfAccount(Long accountNumber, String month, String year);
	public TransactionSuccessResponse transferAmountWithPhoneNumber(TransferAmountWithPhoneNumberRequestDto transferAmountRequest);
}
