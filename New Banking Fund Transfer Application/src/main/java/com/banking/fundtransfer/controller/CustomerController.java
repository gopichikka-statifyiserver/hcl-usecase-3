package com.banking.fundtransfer.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.fundtransfer.dto.CustomerRegistrationDto;
import com.banking.fundtransfer.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.banking.fundtransfer.model.Customer;
import com.banking.fundtransfer.service.CustomerService;

@RestController
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/register-customer")
	public ResponseEntity<Customer> customerRegistration(@Valid @RequestBody CustomerRegistrationDto customerRegistrationDto) {	
		Customer createdCustomer = customerService.registerNewCustomer(customerRegistrationDto);
		return ResponseEntity.status(HttpStatus.CREATED).header("desc", "Successfully created new customer").body(createdCustomer);
	}
	
	@GetMapping("/customers/phonenumber/{phoneNumber}/validate")
	public ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidationWithPhoneNumber(@PathVariable String phoneNumber) {
		CustomerValidationWithPhoneNumberResponseDto customerValidationResponse = customerService.validateCustomerByPhoneNumber(phoneNumber);
		return ResponseEntity.status(HttpStatus.OK).body(customerValidationResponse);
	}

}
