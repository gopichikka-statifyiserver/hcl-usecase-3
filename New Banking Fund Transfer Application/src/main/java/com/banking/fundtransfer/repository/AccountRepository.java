package com.banking.fundtransfer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banking.fundtransfer.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	
	public Optional<Account> findByCustomer_phoneNumber(String phoneNumber);

}
