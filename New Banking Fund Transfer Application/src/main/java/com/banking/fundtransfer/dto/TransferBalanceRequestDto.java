package com.banking.fundtransfer.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TransferBalanceRequestDto {

	@Min(value = 1, message = "From Account number is a mandatory field. Please give valid account number")
	private Long fromAccountNumber;
	@Min(value = 1, message = "To Account number is a mandatory field. Please give valid account number")
	private Long toAccountNumber;
	@Min(value = 1, message = "Amount to be transferred is a mandatory field and should be of minimum 1 rupee")
	private BigDecimal amountToBeTransferred;
	private Date dateOfTransaction;
	private String comment;
	
	
	
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	public Long getFromAccountNumber() {
		return fromAccountNumber;
	}
	public void setFromAccountNumber(Long fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}
	public Long getToAccountNumber() {
		return toAccountNumber;
	}
	public void setToAccountNumber(Long toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}
	
	

}
