package com.banking.fundtransfer.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CustomerRegistrationDto {
	
    @NotBlank(message = "First name is a mandatory field")
	private String firstName;
    @NotBlank(message = "Last name is a mandatory field")
	private String lastName;
    @Min(value = 13, message = "Minimum age required for the customer is 13")
    @Max(value = 150, message = "Maximum age supported for the customer is 150")
    private int age;
    @NotBlank(message = "Address is a mandatory field")
    private String address;
    @NotBlank(message = "customer phone number is a mandatory field")
	private String phoneNumber;
    @NotBlank(message = "customer email is a mandatory field")
    @Email
	private String email;    
    @NotBlank(message = "uan is a mandatory field")
	private String uanNumber;
    @NotBlank(message = "password is a mandatory field")
    private String password;
    @Valid	
	private AccountRegistrationDto accountRegistrationDto;
	
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	public String getAddress() {
		return address;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public AccountRegistrationDto getAccountRegistrationDto() {
		return accountRegistrationDto;
	}
	public void setAccountRegistrationDto(AccountRegistrationDto accountRegistrationDto) {
		this.accountRegistrationDto = accountRegistrationDto;
	}
	public CustomerRegistrationDto() {
		super();		
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUanNumber() {
		return uanNumber;
	}
	public void setUanNumber(String uanNumber) {
		this.uanNumber = uanNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
