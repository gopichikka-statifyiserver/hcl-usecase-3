package com.banking.fundtransfer.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AccountUpdationDto {
	
	@NotNull(message = "account number is a mandatory field")
	@Min(1)
	private long accountNumber;
	@NotNull(message = "please specify the amount to be transferred")
	@Min(1)
	private BigDecimal addMoneyToAccount;
	
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getAddMoneyToAccount() {
		return addMoneyToAccount;
	}
	public void setAddMoneyToAccount(BigDecimal addMoneyToAccount) {
		this.addMoneyToAccount = addMoneyToAccount;
	}
	
	
}
