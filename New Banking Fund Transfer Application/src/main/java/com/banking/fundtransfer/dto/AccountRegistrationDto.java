package com.banking.fundtransfer.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class AccountRegistrationDto {
	
	@NotBlank(message = "bank name is a mandatory field")
	private String bankName;
	@NotBlank(message = "account type is a mandatory field")
	private String accountType;
	@Min(1000)
	private BigDecimal currentBalance;
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	
	
	

}
