package com.banking.fundtransfer.exception;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ControllerAdvisor {

	@ExceptionHandler(AccountException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> accountException(AccountException accountException) {
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(accountException.getMessage());
	}
	
	@ExceptionHandler(SQLException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> sqlException(SQLException sqlException) {
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(sqlException.getMessage());
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<String> customerIdNotFoundException(AccountNotFoundException accountNotFoundException) {
		return ResponseEntity
		        .status(HttpStatus.NOT_FOUND)
		        .body(accountNotFoundException.getMessage());
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<String> customerNotFound(CustomerNotFoundException customerNotFoundException) {
		return ResponseEntity
		        .status(HttpStatus.NOT_FOUND)
		        .body(customerNotFoundException.getMessage());
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<String> uniqueKeyException(DataIntegrityViolationException dataException) {
		String errorMsg = "Couldn't process the request as there is a duplicate record or a bad constraint with the given details";
		return ResponseEntity
		        .status(HttpStatus.UNPROCESSABLE_ENTITY)
		        .body(errorMsg);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(errors);
    }
}
