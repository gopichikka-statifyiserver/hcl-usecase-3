package com.banking.fundtransfer.exception;

public class AccountNotFoundException extends RuntimeException {

	public AccountNotFoundException(String errorMessage) {
		super(errorMessage);
	}

	
}
